_______________________________________________________________________________________________________________________________________________
ASUS PRIME H570M-PLUS Intel Socket LGA1200 matična plošča | CENA:156,99 €
-----------------------------------------------------------------------------------------------------------------------------------------------
-Matična ploščica z podnožjem LGA 1200 primarno namenjeno za intel procesorje
desete in enajste generacije.

-Vsebuje 2 USB 3.0 in USB 2.0 priključka, HDMI in Ethernet priključek. 
Vsebuje štiri DD4 vtičniki.

-Ima vgrajen hldailnik (VRM Heatsink) za hlajenje drugih komponent.